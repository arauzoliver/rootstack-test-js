import Vue from 'vue'
import Vuex from 'vuex'
/* import axios from 'axios' */

const axios = require('axios')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    data: [],
    user:[],
    mapslist: [],
    currentmap:{}
  },
  mutations: {
    setToken(state, payload){
      state.token = payload
    },
    setData(state, resp){
      state.data = resp
    },
    setUserInfo(state, user){
      state.user = user
    },
    setMap(state, payload){
      state.currentmap = payload
     /*  console.log('mutation setmap',payload) */
    }
  },
  actions: {
  login2({commit},payload){
      axios.post('http://coding-test.rootstack.net/api/auth/login',payload)
      .then(function (response) {
        /* console.log('respuesta login2',response) */
        const resDB = response.data
        commit('setToken', resDB.access_token)
        /* console.log('mira user',response.config.data) */
        localStorage.setItem('token', resDB.access_token)
      })
      .catch(function (error) {
        console.log("login2 resp", error);
      });
    },
    getToken({commit}){
      if(localStorage.getItem('token')){
        commit('setToken', localStorage.getItem('token'))
      }else{
        commit('setToken', null)
      }
    },
    getData({commit}){
      axios.get('http://coding-test.rootstack.net/api/jobs', {
        headers: {
          'Authorization': 'Bearer '+this.state.token
        }
      })
      .then(function (response) {
        commit('setData', response.data.data)
        console.log('data set',response);
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      }); 
    },
    getUserInfo({commit}){
      axios.get('http://coding-test.rootstack.net/api/auth/me', {
        headers: {
          'Authorization': 'Bearer '+this.state.token
        }
      })
      .then(function (response) {
        commit('setUserInfo', response.data)
        console.log('user set',response.data)
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      }); 
    },
    mapear({commit},payload){
      commit('setMap', payload)
      console.log('map lat lng setted',payload);
    },
    logout({commit}){
      localStorage.removeItem('token')
      commit('setToken', null)
    }
  },
  modules: {
  }
})
